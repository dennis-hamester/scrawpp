scrawpp Changelog
=================

Version 0.2.1, released on 23.04.2016
-------------------------------------
 * Require `scraw` 0.2.1

Version 0.2.0, released on 13.04.2016
-------------------------------------
* `Context` and `Controller` gained a new function named `scraw_obj()`, which
  returns the underlying `scraw` object (`scraw_context_t*` or
  `scraw_controller_t*`).
* Added `std::hash` specializations.

Version 0.1.0, released on 23.03.2016
-------------------------------------
First release
