Overview
========

`scrawpp` is a simple header-only C++ wrapper for
[scraw](https://gitlab.com/dennis-hamester/scraw).

The current version is 0.2.1 and it depends on `scraw` version 0.2.1.

There is no documentation at the moment. Please take a look at the original
[scraw documentation](https://dennis-hamester.gitlab.io/scraw/). It should still
be useful as `scrawpp` is an almost 1-to-1 wrapper.


Packages
========
* Arch Linux: https://aur.archlinux.org/packages/scrawpp/
* Arch Linux (git version): https://aur.archlinux.org/packages/scrawpp-git/


License
=======
`scrawpp` is distributed under the ISC license. This project is not affiliated
with Valve Cooperation.

```
Copyright (c) 2016, Dennis Hamester dennis.hamester@startmail.com

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
