// Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

#include <scrawpp/errors.hpp>
#include <algorithm>
#include <cassert>
#include <functional>
#include <memory>
#include <string>
#include <scraw/scraw.h>

#ifndef _SCRAWPP_CONTROLLER_HPP_
#define _SCRAWPP_CONTROLLER_HPP_

namespace scraw {

class context;

enum class controller_type {
    unknown,
    wired,
    wireless,
};

enum class feedback_side {
    left,
    right,
};

enum class calibrate_component {
    trackpads,
    joystick,
    imu,
};

struct controller_info {
    controller_type type = controller_type::unknown;
    uint16_t product_id = 0;
    uint32_t bootloader_ts = 0;
    uint32_t firmware_ts = 0;
    uint32_t radio_ts = 0;
    std::string serial_number;
    uint32_t receiver_firmware_ts = 0;
    std::string receiver_serial_number;
};

struct controller_config {
    uint16_t idle_timeout = 0;
    bool imu = false;
};

class controller {
public:
    using on_state_change_fn = void (controller, const scraw_controller_state_t&);

    controller() = default;

    controller(scraw_controller_t* scraw_ctrl)
        : _scraw_ctrl(scraw_ctrl) {
        if(!_scraw_ctrl) {
            return;
        }

        retain();

        void* user_data = nullptr;
        throw_if_error(scraw_controller_get_user_data(_scraw_ctrl, &user_data));

        if(!user_data) {
            int scraw_res = scraw_controller_on_destroy(_scraw_ctrl, destroy_cb);
            if(scraw_res != SCRAW_RESULT_SUCCESS) {
                release();
                throw_if_error(scraw_res);
            }

            auto on_state_change = new std::function<on_state_change_fn>();
            scraw_res = scraw_controller_set_user_data(_scraw_ctrl, on_state_change);
            if(scraw_res != SCRAW_RESULT_SUCCESS) {
                delete on_state_change;
                release();
                throw_if_error(scraw_res);
            }

            scraw_res = scraw_controller_on_state_change(_scraw_ctrl, state_change_cb);
            if(scraw_res != SCRAW_RESULT_SUCCESS) {
                release();
                throw_if_error(scraw_res);
            }
        }
    }

    ~controller() {
        release();
    }

    controller(const controller& rhs)
        : _scraw_ctrl(rhs._scraw_ctrl) {
        retain();
    }

    controller(controller&& rhs)
        : _scraw_ctrl(rhs._scraw_ctrl) {
        rhs._scraw_ctrl = nullptr;
    }

    controller& operator=(const controller& rhs) {
        if(this != &rhs) {
            release();
            _scraw_ctrl = rhs._scraw_ctrl;
            retain();
        }

        return *this;
    }

    controller& operator=(controller&& rhs) {
        if(this != &rhs) {
            release();
            _scraw_ctrl = rhs._scraw_ctrl;
            rhs._scraw_ctrl = nullptr;
        }

        return *this;
    }

    scraw_controller_t* scraw_obj() const {
        return _scraw_ctrl;
    }

    bool operator==(const controller& rhs) const {
        return _scraw_ctrl == rhs._scraw_ctrl;
    }

    bool operator!=(const controller& rhs) const {
        return _scraw_ctrl != rhs._scraw_ctrl;
    }

    bool is_available() {
        if(!_scraw_ctrl) {
            return false;
        }

        int scraw_res = scraw_controller_is_available(_scraw_ctrl);
        switch(scraw_res) {
        case SCRAW_RESULT_SUCCESS:
            return true;

        case SCRAW_RESULT_ERROR_DISCONNECTED:
            return false;

        default:
            throw_if_error(scraw_res);
            throw error(scraw_res); // Unreachable
        }
    }

    controller_type type() {
        int type = 0;
        throw_if_error(scraw_controller_get_type(_scraw_ctrl, &type));

        switch(type) {
        case SCRAW_CONTROLLER_TYPE_WIRED:
            return controller_type::wired;

        case SCRAW_CONTROLLER_TYPE_WIRELESS:
            return controller_type::wireless;

        default:
            return controller_type::unknown;
        }
    }

    controller_info info() {
        scraw_controller_info_t scraw_info;
        scraw_info.size = sizeof(scraw_controller_info_t);
        throw_if_error(scraw_controller_get_info(_scraw_ctrl, &scraw_info));

        controller_info info;
        switch(scraw_info.type) {
        case SCRAW_CONTROLLER_TYPE_WIRED:
            info.type = controller_type::wired;
            break;

        case SCRAW_CONTROLLER_TYPE_WIRELESS:
            info.type = controller_type::wireless;
            break;

        default:
            info.type = controller_type::unknown;
            break;
        }

        info.product_id = scraw_info.product_id;
        info.bootloader_ts = scraw_info.bootloader_ts;
        info.firmware_ts = scraw_info.firmware_ts;
        info.radio_ts = scraw_info.radio_ts;
        info.serial_number = scraw_info.serial_number;
        info.receiver_firmware_ts = scraw_info.receiver_firmware_ts;
        info.receiver_serial_number = scraw_info.receiver_serial_number;

        return info;
    }

    std::function<on_state_change_fn>& on_state_change() {
        void* user_data = nullptr;
        throw_if_error(scraw_controller_get_user_data(_scraw_ctrl, &user_data));

        auto on_state_change = reinterpret_cast<std::function<on_state_change_fn>*>(user_data);
        return *on_state_change;
    }

    bool get_state(scraw_controller_state_t& state) {
        bool changed = false;
        throw_if_error(scraw_controller_get_state(_scraw_ctrl, &state, &changed));
        return changed;
    }

    void configure(const controller_config& config) {
        scraw_controller_config_t scraw_config;
        scraw_config.size = sizeof(scraw_controller_config_t);
        scraw_config.idle_timeout = config.idle_timeout;
        scraw_config.imu = config.imu;

        throw_if_error(scraw_controller_configure(_scraw_ctrl, &scraw_config));
    }

    void lizard_buttons(bool enable) {
        throw_if_error(scraw_controller_lizard_buttons(_scraw_ctrl, enable));
    }

    void enable_lizard_analog() {
        throw_if_error(scraw_controller_enable_lizard_analog(_scraw_ctrl));
    }

    void feedback(feedback_side side, uint16_t amplitude, uint16_t period, uint16_t count) {
        int scraw_side = (side == feedback_side::left) ? SCRAW_FEEDBACK_SIDE_LEFT : SCRAW_FEEDBACK_SIDE_RIGHT;
        throw_if_error(scraw_controller_feedback(_scraw_ctrl, scraw_side, amplitude, period, count));
    }

    void set_led_brightness(uint8_t percent) {
        throw_if_error(scraw_controller_set_led_brightness(_scraw_ctrl, percent));
    }

    template <typename Iter>
    void set_audio_indices(Iter indices_begin, Iter indices_end) {
        assert((indices_end - indices_begin) == 16);

        uint8_t indices[16];
        std::copy(indices_begin, indices_end, indices);

        throw_if_error(scraw_controller_set_audio_indices(_scraw_ctrl, indices));
    }

    void play_audio(uint8_t index) {
        throw_if_error(scraw_controller_play_audio(_scraw_ctrl, index));
    }

    void turn_off() {
        throw_if_error(scraw_controller_turn_off(_scraw_ctrl));
    }

    void calibrate(calibrate_component component) {
        int scraw_component;
        switch(component) {
        case calibrate_component::trackpads:
            scraw_component = SCRAW_CALIBRATE_TRACKPAD;
            break;

        case calibrate_component::joystick:
            scraw_component = SCRAW_CALIBRATE_JOYSTICK;
            break;

        case calibrate_component::imu:
            scraw_component = SCRAW_CALIBRATE_IMU;
            break;
        }

        throw_if_error(scraw_controller_calibrate(_scraw_ctrl, scraw_component));
    }

private:
    static void destroy_cb(scraw_controller_t* scraw_ctrl) {
        void* user_data = nullptr;
        if(scraw_controller_get_user_data(scraw_ctrl, &user_data) != SCRAW_RESULT_SUCCESS) {
            return;
        }

        auto on_state_change = reinterpret_cast<std::function<on_state_change_fn>*>(user_data);
        delete on_state_change;
    }

    static void state_change_cb(scraw_controller_t* scraw_ctrl, const scraw_controller_state_t* state) {
        void* user_data = nullptr;
        if(scraw_controller_get_user_data(scraw_ctrl, &user_data) != SCRAW_RESULT_SUCCESS) {
            return;
        }

        auto on_state_change = reinterpret_cast<std::function<on_state_change_fn>*>(user_data);
        if(!*on_state_change) {
            return;
        }

        (*on_state_change)(controller(scraw_ctrl), *state);
    }

    void retain() {
        if(_scraw_ctrl) {
            scraw_controller_retain(_scraw_ctrl);
        }
    }

    void release() {
        if(_scraw_ctrl) {
            scraw_controller_release(_scraw_ctrl);
        }
    }

    scraw_controller_t* _scraw_ctrl = nullptr;
};

} // namespace scraw

namespace std {

template <>
struct hash<scraw::controller> {
    std::size_t operator()(const scraw::controller& ctrl) const {
        return std::hash<scraw_controller_t*>()(ctrl.scraw_obj());
    }
};

} // namespace std

#endif // _SCRAWPP_CONTROLLER_HPP_
