// Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

#include <exception>
#include <string>
#include <scraw/scraw.h>

#ifndef _SCRAWPP_ERRORS_HPP_
#define _SCRAWPP_ERRORS_HPP_

namespace scraw {

struct error
    : virtual std::exception {
    error() = default;

    error(int code)
        : error_code(code) {
    }

    int error_code;
};

struct unknown_error
    : virtual error {
    unknown_error()
        : error(SCRAW_RESULT_ERROR_UNKNOWN) {
    }
};

struct disconnected_error
    : virtual error {
    disconnected_error()
        : error(SCRAW_RESULT_ERROR_DISCONNECTED) {
    }
};

struct not_supported_error
    : virtual error {
    not_supported_error()
        : error(SCRAW_RESULT_NOT_SUPPORTED) {
    }
};

static inline void throw_if_error(int scraw_res) {
    switch(scraw_res) {
    case SCRAW_RESULT_SUCCESS:
        return;

    case SCRAW_RESULT_ERROR_UNKNOWN:
        throw unknown_error();

    case SCRAW_RESULT_ERROR_DISCONNECTED:
        throw disconnected_error();

    case SCRAW_RESULT_NOT_SUPPORTED:
        throw not_supported_error();

    default:
        throw error(scraw_res);
    }
}

} // namespace scraw

#endif // _SCRAWPP_ERRORS_HPP_
