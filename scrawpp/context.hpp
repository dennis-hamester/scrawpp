// Copyright (c) 2016, Dennis Hamester <dennis.hamester@startmail.com>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
// REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
// FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
// INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
// LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
// OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
// PERFORMANCE OF THIS SOFTWARE.

#include <scrawpp/controller.hpp>
#include <scrawpp/errors.hpp>
#include <functional>
#include <utility>
#include <vector>
#include <scraw/scraw.h>

#ifndef _SCRAWPP_CONTEXT_HPP_
#define _SCRAWPP_CONTEXT_HPP_

namespace scraw {

class context {
public:
    context() {
        throw_if_error(scraw_context_create(&_scraw_ctx, nullptr, nullptr));
    }

    template <typename Gained, typename Lost>
    context(Gained&& on_controller_gained, Lost&& on_controller_lost)
        : _on_ctrl_gained(std::forward<Gained>(on_controller_gained)),
          _on_ctrl_lost(std::forward<Lost>(on_controller_lost)) {
        throw_if_error(scraw_context_create(&_scraw_ctx, &event_handler, this));
    }

    ~context() {
        destroy();
    }

    context(const context&) = delete;

    context(context&& rhs)
        : _scraw_ctx(rhs._scraw_ctx),
          _on_ctrl_gained(move(rhs._on_ctrl_gained)),
          _on_ctrl_lost(move(rhs._on_ctrl_lost)) {
        rhs._scraw_ctx = nullptr;
    }

    context& operator=(const context&) = delete;

    context& operator=(context&& rhs) {
        if(_scraw_ctx != rhs._scraw_ctx) {
            destroy();
            _scraw_ctx = rhs._scraw_ctx;
            _on_ctrl_gained = move(rhs._on_ctrl_gained);
            _on_ctrl_lost = move(rhs._on_ctrl_lost);
            rhs._scraw_ctx = nullptr;
        }

        return *this;
    }

    scraw_context_t* scraw_obj() const {
        return _scraw_ctx;
    }

    void handle_events(long timeout_ms) {
        scraw_context_handle_events(_scraw_ctx, timeout_ms);
    }

    std::vector<controller> list_controllers() {
        scraw_controller_t** list = nullptr;
        int num_ctrls = 0;
        scraw_context_list_controllers(_scraw_ctx, &list, &num_ctrls);

        std::vector<controller> result(list, &list[num_ctrls]);
        scraw_context_free_controller_list(list);

        return result;
    }

private:
    static void event_handler(scraw_context_t* scraw_ctx, int event, void* data, void* user_data) {
        context* ctx = reinterpret_cast<context*>(user_data);

        switch(event) {
        case SCRAW_EVENT_CONTROLLER_GAINED:
            if(ctx->_on_ctrl_gained) {
                ctx->_on_ctrl_gained(controller(reinterpret_cast<scraw_controller_t*>(data)));
            }
            break;

        case SCRAW_EVENT_CONTROLLER_LOST:
            if(ctx->_on_ctrl_lost) {
                ctx->_on_ctrl_lost(controller(reinterpret_cast<scraw_controller_t*>(data)));
            }
            break;
        }
    }

    void destroy() {
        if(_scraw_ctx) {
            scraw_context_destroy(_scraw_ctx);
            _scraw_ctx = nullptr;
        }
    }

    scraw_context_t* _scraw_ctx = nullptr;
    std::function<void (controller ctrl)> _on_ctrl_gained;
    std::function<void (controller ctrl)> _on_ctrl_lost;
};

} // namespace scraw

namespace std {

template <>
struct hash<scraw::context> {
    std::size_t operator()(const scraw::context& ctx) const {
        return std::hash<scraw_context_t*>()(ctx.scraw_obj());
    }
};

} // namespace std

#endif // _SCRAWPP_CONTEXT_HPP_
